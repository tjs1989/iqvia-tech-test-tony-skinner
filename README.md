# Tony Skinner's IQVIA Technical Test

## Fixing the broken UI tests
-  I have chosen to use the Python/Behave setup to complete this task
-  The tests are located in `tasks/python/behave/features/steps/login.py`

### Making changes to improve the tests

We have to wait for the page transition from when we login to the page displaying the welcome message. This is because the login field and the welcome
message are in the same HTML div with the same ID. We therefore have to wait for the absence of the login form, or we have to ask the
developers for a change in application behaviour. From my personal experience, introducing a webdriver wait method before the developmental
change is a great way forward. With a web driver wait we only wait for the set time until the page changes state, otherwise we always wait
for the same amount of time with the use of `time.sleep`.

It may also be sufficient to just implement the web driver wait and not make a developmental change, and this is always a discussion piece
for the product team as a whole.

### Improving the test coverage

I have implemented static login credentials examples into the tests. As a future improvement piece we could implement data tables in feature files so that we can test multiple failure scenarios, 
particularly with the invalid login scenario. We can test things like alpha numeric, number and alphabet password combinations.
Additionally we could also test things such SQL statements i.e.  `SELET * from USERS` to see if these are escaped by the server.

Depending on the sensitivity of test accounts and passwords (I have worked in organisations where they are not so sensitive) we could
have the username and password entry data being pulled in from a secure file, as opposed to being exposed in plain text on the feature
file.

### Improving test stability

All of the elements have been found using Selenium's xpath locating mechanism. Xpath engines differ per web browser, and they also take
a while longer to review and process the DOM. This is well documented in the Selenium community. Furthermore if the web app undergoes changes
(which it always will as your product will be continuously iterated and improved upon) then the element locators will need to be updated. This
is so that the elements can be targeted at their new space on a page.

In my experience, the best way to overcome the problems above is with the use of CSS selectors. We asked the developers (and even added some ourselves)
for custom css attributes to be added to each element of a page. This way if the element moves around the page then Selenium would always find it.
I would therefore request changes to the development of this application with the product team to support this.

## Fixing the broken API tests

The Postman collection is located at `tasks/postman/collections/WorldsBestApp.json`

 

  

