Feature: Login
  In order to use the app the user must be able to Login

  Background:
    Given the user navigates to the worlds best app

  Scenario: Login Success
    When the user enters username test@drugdev.com
    And the user enters password supers3cret
    And clicks Login
    Then the user is presented with a welcome message Welcome Dr I Test

  Scenario: Login Incorrect credentials
    When the user enters username testing@drugdev.com
    And the user enters password cucumbers
    And clicks Login
    Then the user is presented with a error message
