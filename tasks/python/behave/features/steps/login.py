import time

from behave import *
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


@given(u'the user navigates to the worlds best app')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')


@when(u'the user enters username {username}')
def step_impl(context, username):
    context.browser.find_element(By.XPATH, '//*[@id="login-form"]/fieldset/div[2]/input').send_keys(username)


@when(u'the user enters password {password}')
def step_impl(context, password):
    context.browser.find_element(By.XPATH, '//*[@id="login-form"]/fieldset/div[3]/input').send_keys(password)


@when(u'clicks Login')
def step_impl(context):
    context.browser.find_element(By.XPATH, '//*[@id="login-form"]/fieldset/div[4]/button').click()
    time.sleep(0.3)


@then(u'the user is presented with a welcome message {message}')
def step_impl(context, message):
    receivedmessage = context.browser.find_element(By.XPATH, '/html/body/article').text

    try:
        assert receivedmessage == message
    except:
        raise Exception(
            "Welcome message was not present or correct. Expected " + message + " and got " + receivedmessage)

@then(u'the user is presented with a error message')
def step_impl(context):
    try:
        context.browser.find_element(By.XPATH, '//*[@id="login-error-box"]')
    except NoSuchElementException:
        raise Exception("Login Error box not found")
