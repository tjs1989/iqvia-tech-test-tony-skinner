from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys


def before_all(context):
    context.browser = webdriver.Chrome()

def after_all(context):
    context.browser.quit()